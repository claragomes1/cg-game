using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gm;
    private int vidas = 3;
    private int vacinas = 0;
    public GameOverScreen GameOverScreen;
    public WinScreen WinScreen;

    // Inicia antes do start
    void Awake()
    {
        if(gm == null){//Garantir que só tem um objeto na cena
            gm = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }
    void Start(){
        AtualizaHud();
        AtualizaHud1();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetVidas(int vida){
        vidas += vida;
        AtualizaHud();
    }

    public int GetVidas(){
        return vidas;
    }
    public void SetVacinas(int vacina){
        vacinas += vacina;
        AtualizaHud1();
    }
    public int GetVacinas(){
        return vacinas;
    }
    public void AtualizaHud(){
        GameObject.Find("MaskText").GetComponent<Text>().text=vidas.ToString();
    }
    public void AtualizaHud1(){
        GameObject.Find("VaccineText").GetComponent<Text>().text=vacinas.ToString();
    }
    public void GameOver(){
        //SceneManager.LoadScene("SampleScene");
        GameOverScreen.Setup();
    }
    public void Win(){
        //SceneManager.LoadScene("SampleScene");
        WinScreen.Setup();
    }
}
