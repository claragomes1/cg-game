using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private CharacterController controller;
    private float jumpVelocity;

    public float rayRadius;//Distancia
    public LayerMask layer;
    public LayerMask maskLayer;
    public LayerMask vaccineLayer;

    public float speed;
    public float jumpHeight;
    public float gravity;

    public float horizontalSpeed;
    private bool isMovingLeft;
    private bool isMovingRight;


    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = Vector3.forward * speed;

        if (controller.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jumpVelocity = jumpHeight;
            }

        }
        else
        {
            jumpVelocity -= gravity;
        }

        direction.y = jumpVelocity;
        controller.Move(direction * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow))//para esquerda
        {
            GameObject aux = GameObject.Find("Player");
            aux.transform.Translate(new Vector3((-0.1f * horizontalSpeed), 0.0f, 0.0f));
        }
        if (Input.GetKey(KeyCode.RightArrow))//para direita
        {
            GameObject aux = GameObject.Find("Player");
            aux.transform.Translate(new Vector3((0.1f * horizontalSpeed), 0.0f, 0.0f));
        }

        onCollision();

    }

    IEnumerator LeftMove()
    {
        for(float i = 0; i < 10; i += 0.1f)
        {
            controller.Move(Vector3.left * Time.deltaTime * horizontalSpeed);
            yield return null;
        }

        isMovingLeft = true;
    }

    IEnumerator RightMove()
    {
        for (float i = 0; i < 10; i += 0.1f)
        {
            controller.Move(Vector3.right * Time.deltaTime * horizontalSpeed);
            yield return null;
        }

        isMovingRight = false;
    }
    void onCollision(){
        RaycastHit hit;
        RaycastHit maskHit;
        RaycastHit vaccineHit;


        //Posição de origem: Raio a partir do personagem,
        // transform.TransformDirection(Vector3.forward) -> para frente
        //posição de direção: forward é a posição de 1 do eixo z
        //out hit: é onde vou armazenar o objeto que bati
        //distância
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward+ new Vector3(0,5f,0)), out hit, rayRadius, layer)){//aglomeração
            Debug.Log("bateu 2!");
            gameObject.GetComponent<PlayerLife>().PerdeVida();

        }
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward + new Vector3(0,1f,0)), out maskHit, rayRadius, maskLayer)){//mascara
            Debug.Log("bateu 1!");
            Destroy(maskHit.transform.gameObject);
            gameObject.GetComponent<PlayerLife>().GanhaVida();

        }
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward+ new Vector3(0,5f,0)), out vaccineHit, rayRadius, vaccineLayer)){//vacina
            Debug.Log("bateu 3!");
            Destroy(vaccineHit.transform.gameObject);
            gameObject.GetComponent<PlayerLife>().GanhaVacina();
        }
        if(GameManager.gm.GetVidas()==0){
            Debug.Log("Morreuuuuuu");
            GameManager.gm.GameOver();
        }
        if(GameManager.gm.GetVacinas()>=2){
            Debug.Log("Ganhouuuuuu");
            GameManager.gm.Win();
        }

    }


}
