using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{   public int teste;
    Animator anim;
    bool vivo = true;
    // Start is called before the first frame update
    void Start()
    {
        anim =gameObject.GetComponent<Animator>();
        GameManager.gm.AtualizaHud();
        GameManager.gm.AtualizaHud1();
    }

    // Update is called once per frame
    void Update()
    {
        Invoke("verificaVida", 8);
       //StartCoroutine(verificaVida());

    }

    public void PerdeVida(){
        if(GameManager.gm.GetVidas()<=0){
            vivo = false;
            Debug.Log("morreu");
        }
        if(vivo && GameManager.gm.GetVidas()>0){
            vivo = false;
            //anim.SetTrigger("nome no trigger");
            GameManager.gm.SetVidas(-1);
        }

    }

    public void GanhaVida(){

        if(GameManager.gm.GetVidas()<3){
            //vivo = false;
            //anim.SetTrigger("nome no trigger");
            GameManager.gm.SetVidas(+1);
        }

    }

    public void GanhaVacina(){
        GameManager.gm.SetVacinas(+1);

        //if(GameManager.gm.GetVacinas()>2){
            //anim.SetTrigger("nome no trigger de ganhar jogo");

       // }

    }

    public void verificaVida(){

        if(GameManager.gm.GetVidas()>=0){
            vivo = true;
        }
    }
    /* public void verificaVida2(){
        yield return new WaitForSeconds(3);
        renderer.enabled=true;
        StartCoroutine(verificaVida());
    }*/

    public void Reset(){
        if(GameManager.gm.GetVidas() >= 0){
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
